﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace lab6oop
{
    [XmlInclude(typeof(FruitPlant))]
    [XmlInclude(typeof(Fern))]
    [Serializable]
    public abstract class Plant
    {
        public string Label { get; set; }            // Метка
        public int Age { get; set; }                 // Возраст
        public string Status { get; set; }           // Статус (Жив, Погиб и т.д)
    }

    interface INeedCut  // Требует стрижки
    {
        public int NoCutTime { get; }
        public bool NeedCut { get; protected set; }
        public void Cut() { NeedCut = false; }
    }

    interface INeedChangingSoil   // Требует смены почвы
    {
        public int NoChangingSoilTime { get; }
        public bool NeedChangingSoil { get; protected set; }
        public void ChangingSoil() { NeedChangingSoil = false; }
    }

    interface INeedWatering  // Требуется полив
    {
        public int NoWateringTime { get; }
        public bool NeedWatering { get; protected set; }
        public void Watering() { NeedWatering = false; }
    }
    interface INeedCollectedFruit
    {
        public int NoCollectingFruitTime { get; }
        public bool NeedCollectingFruit { get; protected set; }
        public virtual void CollectingFruit() { NeedCollectingFruit = false; }
    }

    [XmlInclude(typeof(Nefrolepis))]
    [Serializable]
    public abstract class Fern : Plant, INeedCut, INeedWatering
    {
        // INeedCut
        public int NoCutTime { get; set; }
        int INeedCut.NoCutTime => NoCutTime;

        public bool NeedCut { get; set; }
        bool INeedCut.NeedCut { get => NeedCut; set => NeedCut = value; }
        
        public virtual void Cut() { (this as INeedCut).Cut(); }

        // INeedWatering
        public int NoWateringTime { get; set; }
        int INeedWatering.NoWateringTime => NoWateringTime;
        
        public bool NeedWatering { get; set; }
        bool INeedWatering.NeedWatering { get => NeedWatering; set => NeedWatering = value; }

        public virtual void Watering() { (this as INeedWatering).Watering(); }
    }

    [XmlInclude(typeof(MandarinTree))]
    [Serializable]
    public abstract class FruitPlant : Plant, INeedWatering, INeedChangingSoil, INeedCollectedFruit
    {
        public int FruitCount { get; set; }
        
        // INeedWatering
        public int NoWateringTime { get; set; }
        int INeedWatering.NoWateringTime => NoWateringTime;
        
        public bool NeedWatering { get; set; }
        bool INeedWatering.NeedWatering { get => NeedWatering; set => NeedWatering = value; }

        public virtual void Watering() { (this as INeedWatering).Watering(); }

        // INeedChangingSoil
        public int NoChangingSoilTime { get; set; }
        int INeedChangingSoil.NoChangingSoilTime => NoChangingSoilTime;
        public bool NeedChangingSoil { get; set; }
        bool INeedChangingSoil.NeedChangingSoil { get => NeedChangingSoil; set => NeedChangingSoil = value; }

        public virtual void ChangingSoil() { (this as INeedChangingSoil).ChangingSoil(); }

        // INeedCollectedFruit
        public int NoCollectingFruitTime { get; set; }
        int INeedCollectedFruit.NoCollectingFruitTime => NoCollectingFruitTime;

        public bool NeedCollectingFruit { get; set; }
        bool INeedCollectedFruit.NeedCollectingFruit { get => NeedCollectingFruit; set => NeedCollectingFruit = value; }

        public virtual void CollectingFruit() { (this as INeedCollectedFruit).CollectingFruit(); }
    }

    [Serializable]
    public class Nefrolepis : Fern
    {
        public Nefrolepis() { }
        public Nefrolepis(string label)
        {
            // [Intrface]               // [Unit]
            // Plant
            Label = label;
            Age = 0;                    // (Days)
            Status = "Alive";

            // INeedCut
            NoCutTime = 5;              // (Days)
            NeedCut = true;

            // INeedWatering
            NoWateringTime = 1;         // (Days)
            NeedWatering = true;
        }

        public override void Cut()
        {
            base.Watering();
            Console.WriteLine("Был подстрижен Нефролепис");
        }

        public override void Watering()
        {
            base.Watering();
            Console.WriteLine("Был полит Нефролепис");
        }
    }

    [Serializable]
    public class MandarinTree : FruitPlant
    {
        public MandarinTree() { }
        public MandarinTree(String label)
        {
            // [Intrface]               // [Unit]
            // Plant
            Label = label;
            Age = 0;                    // (Days)
            Status = "Alive";

            // FruitPlant
            FruitCount = 0;

            // INeedWatering
            NoWateringTime = 4;         // (Days)
            NeedWatering = true;

            // INeedChangingSoil
            NoChangingSoilTime = 365;   // (Days)
            NeedChangingSoil = false;

            // INeedCollectedFruit
            NoCollectingFruitTime = 5;  // (Days)
            NeedCollectingFruit = false;
        }

        public override void Watering()
        {
            base.Watering();
            Console.WriteLine("Было полито Мандариновое дерево");
        }

        public override void ChangingSoil()
        {
            base.ChangingSoil();
            Console.WriteLine("Было заменена почва для Мандаринового дерева");
        }

        public override void CollectingFruit()
        {
            base.CollectingFruit();
            Console.WriteLine("Было собранно {} мандаринов с дерева", FruitCount);
            FruitCount = 0;
        }
    }

    [Serializable]
    public class PlantDictionary // Общий справочник оранжереи
    {
        public List<Plant> Plants { get; } = new List<Plant>();

        public PlantDictionary() { }
        public PlantDictionary(ICollection<Plant> collection)
        {
            Plants.AddRange(collection);
        }

        public static PlantDictionary load(string path)
        {
            XmlSerializer fmt = new XmlSerializer(typeof(List<Plant>));

            using (Stream st = new FileStream(path, FileMode.Open)) 
            {
                return new PlantDictionary(
                    (List<Plant>)fmt.Deserialize(st)
                    );
            }
        }

        public void save(string path)
        {
            XmlSerializer fmt = new XmlSerializer(typeof(List<Plant>));
            
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                fmt.Serialize(fs, Plants);
            }
        }    
    }

    class Program
    {
        static void Main(string[] args)
        {
            PlantDictionary db = new PlantDictionary();

            string comm;
            while ("Quit" != (comm = Console.ReadLine()))
            {
                switch (comm)
                {
                    case "Add":
                        {
                            Console.Write("Введите тип растения: ");
                            string plant_type = Console.ReadLine();
                            Console.Write("Введите метку: ");
                            string label = Console.ReadLine();

                            switch (plant_type)
                            {
                                case "MandarinTree":
                                    {
                                        db.Plants.Add(new MandarinTree(label));
                                        break;
                                    }
                                case "Nefrolepis":
                                    {
                                        db.Plants.Add(new Nefrolepis(label));
                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Ошибка! Системе неизвестен данный тип");
                                        break;
                                    }
                            }

                            break;
                        }
                    
                    case "Save":
                        {
                            db.save("db.xml");
                            break;
                        }
                    case "Load":
                        {
                            try
                            {
                                db = PlantDictionary.load("db.xml");
                            }
                            catch
                            {
                                Console.WriteLine("Ошибка! Невозможно открыть файл");
                            }
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Ошибка! Неверная комманда");
                            break;
                        }
                }
            }
        }
    }
}
